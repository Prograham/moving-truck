# Moving Truck

An ASCII art application that displays a moving ASCII art truck in your terminal.

This terminal application is run from the command line and written in Python3.*.

### Installation Instructions
Users using Debian-based Linux distributions will find the most ease installing this application.

#### Debian / Ubuntu
`git clone <project url>;
cd <project_directory>;
dpkg -i moving-truck.deb`

#### Other Linux distributions
`git clone <project url>;
cd <project_directory;
python3 moving_truck.py`


USAGE:
	`python3 {current_directory}/moving_truck.py`

OR
	
>Add a symbolic link in Linux to the file.

1. chmod +x {current_directory}/moving_truck.py
2. ln -s {current_directory}/moving_truck.py /usr/bin/truck
