#!/bin/bash

get_package_details() {
	read -p "Package [moving-truck]: " p
	p=${p:-"moving-truck"}
	echo "Package: "$p > moving-truck/DEBIAN/control
        read -p "Version number: " p
        while [ "$p" == "" ]; do
		read -p "Version number: " p
	done
        echo "Version: "$p >> moving-truck/DEBIAN/control
        read -p "Section [custom]: " p
        p=${p:-"custom"}
        echo "Section: "$p >> moving-truck/DEBIAN/control
        read -p "Priority [optional]: " p
        p=${p:-"optional"}
        echo "Priority: "$p >> moving-truck/DEBIAN/control
	read -p "Architecture [all]: " p
        p=${p:-"all"}
        echo "Architecture: "$p >> moving-truck/DEBIAN/control
        read -p "Depends [python3]: " p
        p=${p:-"python3"}
        echo "Depends: "$p >> moving-truck/DEBIAN/control
        read -p "Provides [truck]: " p
        p=${p:-"truck"}
        echo "Provides: "$p >> moving-truck/DEBIAN/control
        read -p "Maintainer [Graham Dinkel]: " p
        p=${p:-"Graham Dinkel"}
        echo "Maintainer: "$p >> moving-truck/DEBIAN/control
        read -p "Description [An ASCII art application that displays a moving ASCII art truck.]: " p
        p=${p:-"An ASCII art application that displays a moving ASCII art truck."}
        echo "Description: "$p >> moving-truck/DEBIAN/control
}

mkdir -p moving-truck/DEBIAN
get_package_details
echo "# do postinstall" > moving-truck/DEBIAN/postinst
chmod +x moving-truck/DEBIAN/postinst
mkdir -p moving-truck/usr/games/truck_files
cp moving_truck.py moving-truck/usr/games/truck_files/
cp truck* moving-truck/usr/games/truck_files/
cp truck  moving-truck/usr/games/truck
chmod +x moving-truck/usr/games/truck
chmod +x moving-truck/usr/games/truck_files/moving_truck.py
dpkg -b moving-truck
rm -rf moving-truck
