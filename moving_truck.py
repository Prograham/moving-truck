#!/usr/bin/python3
import subprocess
import random
import sys
import os
from time import sleep


class DisplayAsciiTruck:
    def __init__(self, ascii_art):
        self.longest_line = 0
        self.terminal_x_y = self.grab_term_xy()
        self.term_x = self.terminal_x_y[1]
        self.term_y = self.terminal_x_y[0]
        self.ascii_art = ascii_art
        self.road_chars = ['.', 'o', '0']

    def grab_term_xy(self):
        output = subprocess.Popen(['stty', 'size'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = output.communicate()

        return [int(stdout.split()[0]), int(stdout.split()[1])]

    def increase_line_indentation(self, line, x):
        line = ' ' * x + line + '\n'
        return line

    def redraw(self, x):
        ascii_art_list = [self.increase_line_indentation(line, x) for line in random.choice(self.ascii_art)]
        ascii_art_list[-1] = ascii_art_list[-1][:-1]
        sleep(.02)
        sys.stdout.write(''.join(ascii_art_list) + '\n')
        sys.stdout.flush()
        sys.stdout.write(self.make_road())
        sys.stdout.flush()

    def make_road(self):
        road_list = [random.choice(self.road_chars) for i in range(self.term_x)]

        return ''.join(road_list)

    def start(self):
        art_length = 50

        sys.stdout.write('\033[?1049h')  # clear screen but preserve terminal
        for x in range(self.term_x - art_length):
            self.redraw(x)
            if self.term_y > 15:
                sys.stdout.write('\n' * int(self.term_y / 2))
            sleep(.04)
        sys.stdout.write('\033[?1049l')  # restore terminal to previous spot
        sys.stdout.flush()

if __name__ == '__main__':
    ascii_art = []
    ascii_file_path = os.path.dirname(os.path.realpath(__file__))
    with open(ascii_file_path + '/truck1.txt', 'r') as f1, open(ascii_file_path + '/truck2.txt', 'r') as f2, open(ascii_file_path + '/truck3.txt', 'r') as f3:
        ascii_art.append(f1.read().splitlines())
        ascii_art.append(f2.read().splitlines())
        ascii_art.append(f3.read().splitlines())

    try:
        truck = DisplayAsciiTruck(ascii_art)
        truck.start()
    except KeyboardInterrupt as e:
        sys.stdout.write('\033[?1049l')  # restore terminal to previous s
